package demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@Slf4j
@SpringBootTest(webEnvironment = NONE,
        properties = {
                "logging.level.org.redisson=trace"
        })
class RadissonTest {
    private static final Logger DOCKER_LOGGER = LoggerFactory.getLogger("DOCKER_LOG");
    private static GenericContainer REDIS_CONTAINER;
    private static int port;

    @BeforeAll
    public static void setupServer() {
        REDIS_CONTAINER = new GenericContainer("redis:latest")
                .withExposedPorts(6379)
                .withClasspathResourceMapping("config", "/etc/config/", BindMode.READ_ONLY)
                .withCommand("/etc/config/redis.conf")
                .withLogConsumer(new Slf4jLogConsumer(DOCKER_LOGGER, true));
        REDIS_CONTAINER.start();
        port = REDIS_CONTAINER.getFirstMappedPort();
        //BlockHoundConfiguration.init();
    }


    @Test
    void shouldSetAndGetValueWithMTLS() throws Exception {
        var trustStoreUrl = ResourceUtils.getURL("classpath:config/client/truststore/truststore.pfx");
        var keyStoreUrl = ResourceUtils.getURL("classpath:config/client/keystore/keystore.pfx");
        Config config = new Config();
        String endpoint = "rediss://" + REDIS_CONTAINER.getContainerIpAddress() + ":" + REDIS_CONTAINER.getFirstMappedPort();
        config.useSingleServer()
                .setSslTruststore(trustStoreUrl.toURI())
                .setSslTruststorePassword("clientpwd")
                .setSslKeystore(keyStoreUrl.toURI())
                .setSslKeystorePassword("clientpwd")
                .setSslEnableEndpointIdentification(false)
                .setAddress(endpoint)
                .setConnectionPoolSize(1)
                .setConnectionMinimumIdleSize(0);

        RedissonClient client = Redisson.create(config);
        client.getBucket("1").set("test");
        assertEquals("test", client.getBucket("1").get());
        client.shutdown();
    }

    @EnableAutoConfiguration
    @Configuration
    public static class TestConfiguration {

    }
}
