Демонстрация работы со Spring Lettuce Reactive & Redisson
==========================================================

Нужен локальный Docker

Redisson
Запуск mvn -Dtest=RadissonTest test

Lettuce Reactive
Запуск mvn -Dtest=ReactiveLettuceTest test

Ключевой момент для Lettuce - нужно настраивать mTLS через LettuceClientConfigurationBuilderCustomizer:

        @Bean
        public LettuceClientConfigurationBuilderCustomizer builderCustomizer() throws Exception {
            var trustStoreUrl = ResourceUtils.getURL("classpath:config/client/truststore/truststore.pfx");
            var keyStoreUrl = ResourceUtils.getURL("classpath:config/client/keystore/keystore.pfx");
            return builder -> builder.clientOptions(ClientOptions.builder()
                    .sslOptions(SslOptions.builder()
                            .keystore(keyStoreUrl,"clientpwd".toCharArray())
                            .truststore(trustStoreUrl,"clientpwd")
                            .build())
                    .build())
                    .useSsl().disablePeerVerification();
        }

Ключевой момент для Redisson - указание протокола rediss и настройка KeyStore и TrustStore в конфигурации
        Config config = new Config();
        String endpoint = "rediss://" + REDIS_CONTAINER.getContainerIpAddress() + ":" + REDIS_CONTAINER.getFirstMappedPort();
        config.useSingleServer()
                .setSslTruststore(trustStoreUrl.toURI())
                .setSslTruststorePassword("clientpwd")
                .setSslKeystore(keyStoreUrl.toURI())
                .setSslKeystorePassword("clientpwd")
                .setSslEnableEndpointIdentification(false)
                .setAddress(endpoint);

Создание ключей
===========================
set PATH=C:\soft\openssl\bin;%PATH%

Очистка
cd src\test\resources
rmdir /S /Q config


mkdir config
mkdir config\server
mkdir config\server\truststore
mkdir config\server\keystore
mkdir config\client
mkdir config\client\keystore
mkdir config\client\truststore

Создаем файл
notepad config\redis.conf

Контент файла
port 0
tls-port 6379
tls-auth-clients yes
tls-cert-file  /etc/config/server/keystore/server.crt
tls-key-file  /etc/config/server/keystore/server.key
tls-ca-cert-file /etc/config/server/truststore/client.crt

openssl genrsa -out config\server\keystore\server.key 2048
openssl genrsa -out config\client\keystore\client.key 2048

openssl rsa -in config\server\keystore\server.key -text -noout
openssl rsa -in config\client\keystore\client.key -text -noout

openssl req -key config\server\keystore\server.key -new -x509 -days 3650 -out config\server\keystore\server.crt  -subj "/CN=DemoServer"
openssl req -key config\client\keystore\client.key -new -x509 -days 3650 -out config\client\keystore\client.crt  -subj "/CN=DemoClient"

copy config\server\keystore\server.crt config\client\truststore\server.crt
copy config\client\keystore\client.crt config\server\truststore\client.crt

keytool -import -noprompt -trustcacerts -file config\client\truststore\server.crt -alias server  -keystore config\client\truststore\truststore.pfx -storepass clientpwd -storetype PKCS12
keytool -list -keystore config\client\truststore\truststore.pfx -storepass clientpwd -storetype PKCS12

openssl pkcs12 -export -in config\client\keystore\client.crt -inkey config\client\keystore\client.key -out config\client\keystore\keystore.pfx -name client-key -password pass:clientpwd
keytool -list -keystore config\client\keystore\keystore.pfx -storepass clientpwd -storetype PKCS12