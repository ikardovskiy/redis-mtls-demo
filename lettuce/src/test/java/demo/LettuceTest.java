package demo;

import io.lettuce.core.ClientOptions;
import io.lettuce.core.SslOptions;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.LettuceClientConfigurationBuilderCustomizer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.util.ResourceUtils;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@Slf4j
@SpringBootTest(webEnvironment = NONE,
        properties = {
                "logging.level.org.springframework.data.redis=trace",
                "logging.level.io.lettuce=trace",
                "logging.level.org.apache.commons=trace"
        })
class LettuceTest {
    private static Logger DOCKER_LOGGER = LoggerFactory.getLogger("DOCKER_LOG");
    private static GenericContainer REDIS_CONTAINER;
    private static int port;

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.redis.host", () -> "localhost");
        registry.add("spring.redis.port", () -> port);
        registry.add("spring.redis.ssl", () -> true);
        registry.add("spring.redis.database", () -> "0");
        registry.add("spring.redis.password", () -> "pwd");
    }

    @BeforeAll
    public static void setupServer() {
        REDIS_CONTAINER = new GenericContainer("redis:latest")
                .withExposedPorts(6379)
                .withClasspathResourceMapping("config", "/etc/config/", BindMode.READ_ONLY)
                .withCommand("/etc/config/redis.conf")
                .withLogConsumer(new Slf4jLogConsumer(DOCKER_LOGGER, true));
        REDIS_CONTAINER.start();
        port = REDIS_CONTAINER.getFirstMappedPort();
        //BlockHoundConfiguration.init();
    }

    @Autowired
    private StringRedisTemplate template;

    @Test
    void shouldSetAndGetValueWithMTLS() {
        template.opsForValue().set("key", "value");
        var actual = template.opsForValue().get("key");
        assertEquals("value", actual);
    }

    @EnableAutoConfiguration
    @Configuration
    public static class TestConfiguration {
        @Bean
        public LettuceClientConfigurationBuilderCustomizer builderCustomizer() throws Exception {
            var trustStoreUrl = ResourceUtils.getURL("classpath:config/client/truststore/truststore.pfx");
            var keyStoreUrl = ResourceUtils.getURL("classpath:config/client/keystore/keystore.pfx");
            return builder -> builder.clientOptions(ClientOptions.builder()
                    .sslOptions(SslOptions.builder()
                            .keystore(keyStoreUrl, "clientpwd".toCharArray())
                            .truststore(trustStoreUrl, "clientpwd")
                            .build())
                    .build())
                    .useSsl().disablePeerVerification();
        }
    }

}
